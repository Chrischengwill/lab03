var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var BearSchema   = new Schema({
    name: String,
    protrait: String,
    email: String,
    age: String,
    gender: String
});

module.exports = mongoose.model('Bear', BearSchema);