/* global $*/
$(function() {
    // GET/READ
    $('#get-button').on('click', function() {
        $.ajax({
            url: '/bearrs',
            contentType: 'application/json',
            success: function(response) {
                var tbodyEl = $('tbody');

                tbodyEl.html('');

                response.bearrs.forEach(function(bearr) {
                    tbodyEl.append('\
                        <tr>\
                            <td class="id">' + bearr.id + '</td>\
                            <td><input type="text" class="name" value="' + bearr.name + '"></td>\
                            <td><input type="text" class="email" value="' + bearr.email + '"></td>\
                            <td><input type="text" class="age" value="' + bearr.age + '"></td>\
                            <td><input type="text" class="gender" value="' + bearr.gender + '"></td>\
                            <td><input type="text" class="protrait" value="' + bearr.protrait + '"></td>\
                            <td>\
                                <button class="update-button">UPDATE/PUT</button>\
                                <button class="delete-button">DELETE</button>\
                            </td>\
                        </tr>\
                    ');
                });
            }
        });
    });

    // CREATE/POST
    $('#create-form').on('submit', function(event) {
        event.preventDefault();

        var createInput = $('#create-input');
        var createInputa = $('#create-inputa');
        var createInputb = $('#create-inputb');
        var createInputc = $('#create-inputc');
       

        $.ajax({
            url: '/bearrs',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ 
                name: createInput.val(),
                email: createInputa.val(),
                age: createInputb.val(),
                gender: createInputc.val()
                
        
                
            }),
            success: function(response) {
                console.log(response);
                createInput.val('');
                $('#get-button').click();
            }
        });
    });

    // UPDATE/PUT
    $('table').on('click', '.update-button', function() {
        var rowEl = $(this).closest('tr');
        var id =rowEl.find('.id').text();
        var newName = rowEl.find('.name').val();
        var newAge = rowEl.find('.age').val();
        var newGender = rowEl.find('.gender').val();
        var newEmail = rowEl.find('.email').val();

        $.ajax({
            url: '/bearrs/'+id,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({ 
                newName: newName,
                newAge: newAge,
                newGender: newGender,
                newEmail: newEmail
            }),
            success: function(response) {
                console.log(response);
                $('#get-button').click();
            }
        });
    });

    //DELETE
    $('table').on('click', '.delete-button', function() {
        var rowEl = $(this).closest('tr');
        var id = rowEl.find('.id').text();

        $.ajax({
            url: '/bearrs/'+id,
            method: 'DELETE',
            contentType: 'application/json',
            success: function(response) {
                console.log(response);
                $('#get-button').click();
            }
        });
    });
});

//****************************************image upload***************************************************