// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var fs = require("fs");
var url = require('url');
var assert = require('assert');


var mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost:27017/bears'); // connect to our database

var Bear     = require('./models/bear');
app.use('/',express.static(__dirname));


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var port = 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)


// router.get('/', function(req, res) {
//      //res.end(fs.readFileSync('test.html','UTF-8'))
//     res.json({ message: 'hooray! welcome to our api!' });   
// });

// more routes for our API will happen here

// on routes that end in /bears
// ----------------------------------------------------
// app.get('/', function (req, res) {
//     res.sendFile(path.join(__dirname + '/static/index.html'));
// });
app.get('/bearrs', function(req, res) {
    res.send({ bearrs: bearrs });
});


var bearrs =[
    {
        id:1,
        name : 'chris',
        email : 'chrischengwill@gmail.com',
        age: '24',
        gender: 'male',
    }
    ];
var currentId = 1;

// router.route('/bears')

//     // create a bear (accessed at POST http://localhost:8080/api/bears)
//     .post(function(req, res) {
        
//         var bear = new Bear(); 
//         // create a new instance of the Bear model
//         bear.name =req.body.name;
//         bear.email = req.body.email;
//         bear.age = req.body.age;
//         bear.gender = req.body.gender;
//         //bear.protrait = req.body.protrait;
        app.post('/bearrs',function(req,res){
            var bearrname = req.body.name;
            var bearremail = req.body.email;
            var bearrage =req.body.age;
            var bearrgender = req.body.gender;
        
        currentId++;
        bearrs.push({
            id: currentId,
            name: bearrname,
            email: bearremail,
            age: bearrage,
            gender: bearrgender
        });
         res.send('Successfully created Bearrs!');
   
    // set the bears name (comes from the request)


        // save the bear and check for errors
        // bear.save(function(err) {
        //     if (err)
        //         res.send(err);

        //     res.json({ message: 'Bear created!' });
        // });
        
    });
    
    // get all the bears (accessed at GET http://localhost:8080/api/bears)
    router.route('/bears')
    .get(function(req, res) {
         
        Bear.find(function(err, bears) {
            if (err)
                res.send(err);

            res.json(bears);
        });
    });

// on routes that end in /bears/:bear_id
// ----------------------------------------------------
router.route('/bears/:bear_id')

    // get the bear with that id (accessed at GET http://localhost:8080/api/bears/:bear_id)
    .get(function(req, res) {
        
        Bear.findById(req.params.bear_id, function(err, bear) {
            if (err)
                res.send(err);
            res.json(bear);
           
        });
    })
    
        // update the bear with this id (accessed at PUT http://localhost:8080/api/bears/:bear_id)
// router.route('/bears/:id')
//     .put(function(req, res) {
//         var newName = req.body.newName;

//         // use our bear model to find the bear we want
//         Bear.findById(req.params.bear_id, function(err, bear) {

//             if (err)
//                 res.send(err);

//             bear.name = req.body.name;
//             bear.email = req.body.email;
//             bear.age = req.body.age;
//             bear.gender = req.body.gender;
//             bear.protrait = req.body.protrait;
//             // update the bears info

//             // save the bear
//             bear.save(function(err) {
//                 if (err)
//                     res.send(err);

//                 res.json({ message: 'Bear updated!' });
//             });

//         });
//     })
    app.put('/bearrs/:id',function(req,res){
        var id =req.params.id;
        var newName = req.body.newName;
        var newAge = req.body.newAge;
        var newGender = req.body.newGender;
        var newEmail = req.body.newEmail;
        var found = false;
        
        
        bearrs.forEach(function(bearr,index) {
            if (!found &&bearr.id ===Number(id)){
                
                bearr.name = newName;
                bearr.age = newAge;
                bearr.gender = newGender;
                bearr.email = newEmail;
            }
        });
        
       res.send('Successfully updated file!');
        });
    
    // bearq.save(function(err) {
    //          if (err)
    //              res.send(err);

    //         res.json({ message: 'Bear updated!' });
        
        
    
    
    
    
        // delete the bear with this id (accessed at DELETE http://localhost:8080/api/bears/:bear_id)
    router.route('/bearsdelete/:bear_id')
    .delete(function(req, res) {
        Bear.remove({
            _id: req.params.bear_id
        }, function(err, bear) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });
app.delete('/bearrs/:id', function(req, res) {
    var id = req.params.id;

    var found = false;

    bearrs.forEach(function(bearr, index) {
        if (!found && bearr.id === Number(id)) {
            bearrs.splice(index, 1);
        }
    });

    res.send('Successfully deleted product!');
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/', router);

app.use(express.static('static'));
// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);

//***************************************image upload****************************************************
// var express = require('express');
// var app = express();
// var bodyParser = require('body-parser');
// var mongoose = require('mongoose');
// var path = require('path');
// app.use(bodyParser.json());
 
//To get the access for the functions defined in index.js class
var routes = require('./routes/imagefile');
 
// // connect to mongo,
// //i have created mongo collection in mlab.com.. the below is my database access url..
// //So make sure you give your connection details..
// //mongoose.connect('mongodb://localhost:27017/bears');
 
// //app.use('/', routes);
 
// //URL : http://localhost:3000/images/
// // To get all the images/files stored in MongoDB
app.get('/images', function(req, res) {
//calling the function from index.js class using routes object..
routes.getImages(function(err, genres) {
if (err) {
throw err;
 
}
res.json(genres);
 
});
});
 
// // URL : http://localhost:3000/images/(give you collectionID)
// // To get the single image/File using id from the MongoDB
app.get('/images/:id', function(req, res) {
 
// //calling the function from index.js class using routes object..
routes.getImageById(req.params.id, function(err, genres) {
if (err) {
throw err;
}
//res.download(genres.path);
res.send(genres.path)
});
});
 
// //app.listen(8080);
 
console.log('Running on port 8080');
